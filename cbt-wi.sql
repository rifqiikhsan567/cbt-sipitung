-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2017 at 12:21 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cbt`
--

-- --------------------------------------------------------

--
-- Table structure for table `cbt_admin`
--

CREATE TABLE `cbt_admin` (
  `Urut` int(11) NOT NULL,
  `XSekolah` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `XTingkat` varchar(3) COLLATE latin1_general_ci NOT NULL,
  `XIp` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `XAlamat` text COLLATE latin1_general_ci NOT NULL,
  `XTelp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XFax` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XEmail` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `XWeb` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `XLogo` text COLLATE latin1_general_ci NOT NULL,
  `XBanner` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `XKepSek` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `XAdmin` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `XPicAdmin` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `XWarna` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XNIPKepsek` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `XNIPAdmin` varchar(30) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cbt_admin`
--

INSERT INTO `cbt_admin` (`Urut`, `XSekolah`, `XTingkat`, `XIp`, `XAlamat`, `XTelp`, `XFax`, `XEmail`, `XWeb`, `XLogo`, `XBanner`, `XKepSek`, `XAdmin`, `XPicAdmin`, `XWarna`, `XStatus`, `XKodeSekolah`, `XNIPKepsek`, `XNIPAdmin`) VALUES
(2, 'SMK Wisata Indonesia', 'SMK', '127.0.0.1', 'ALAMAT', 'PHONE', '-', '-', '-', 'logowi.jpg', 'wibanner.jpg', 'Abdul Munir, H.MA, M.Pd', 'Admin', 'logowi.png', '#336799', '1', 'K0104131', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_audio`
--

CREATE TABLE `cbt_audio` (
  `Urut` int(11) NOT NULL,
  `XMulai` float NOT NULL,
  `XPutar` int(11) NOT NULL,
  `XUserJawab` varchar(50) NOT NULL,
  `XTokenUjian` varchar(10) NOT NULL,
  `XKodeSoal` varchar(50) NOT NULL,
  `XNomerSoal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_jawaban`
--

CREATE TABLE `cbt_jawaban` (
  `Urutan` int(11) NOT NULL,
  `Urut` int(11) NOT NULL,
  `XNomerSoal` int(11) NOT NULL,
  `XKodeUjian` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL,
  `XTokenUjian` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `XA` int(11) NOT NULL,
  `XB` int(11) NOT NULL,
  `XC` int(11) NOT NULL,
  `XD` int(11) NOT NULL,
  `XE` int(11) NOT NULL,
  `XJawaban` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XTemp` text COLLATE latin1_general_ci NOT NULL,
  `XJawabanEsai` text COLLATE latin1_general_ci NOT NULL,
  `XKodeJawab` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `XNilaiJawab` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XNilai` int(11) NOT NULL,
  `XNilaiEsai` float NOT NULL,
  `XRagu` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XMulai` float NOT NULL,
  `XPutar` int(11) NOT NULL,
  `XMulaiV` float NOT NULL,
  `XPutarV` int(11) NOT NULL,
  `XTglJawab` date NOT NULL,
  `XJamJawab` time NOT NULL,
  `XKunciJawaban` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XUserJawab` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Campur` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XSemester` int(1) NOT NULL,
  `XUserPeriksa` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `XTglPeriksa` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XJamPeriksa` varchar(8) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_kelas`
--

CREATE TABLE `cbt_kelas` (
  `Urut` int(11) NOT NULL,
  `XKodeLevel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XNamaKelas` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XStatusKelas` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cbt_kelas`
--

INSERT INTO `cbt_kelas` (`Urut`, `XKodeLevel`, `XNamaKelas`, `XKodeJurusan`, `XKodeKelas`, `XStatusKelas`, `XKodeSekolah`) VALUES
(1, 'XII', 'XII TKJ', 'TKJ', 'XII TKJ', '1', ''),
(2, 'XII', 'XII PH A', 'Akomodasi Perhotelan', 'XII PH', '1', ''),
(3, 'XII', 'XII PH B', 'Akomodasi Perhotelan', 'XII PH', '1', ''),
(4, 'XII', 'XII BG A', 'Jasa Boga', 'XII BG', '1', ''),
(5, 'XII', 'XII BG B', 'Jasa Boga', 'XII BG', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_mapel`
--

CREATE TABLE `cbt_mapel` (
  `Urut` int(11) NOT NULL,
  `XKodeKelas` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XNamaMapel` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `XTglBuat` date NOT NULL,
  `XPersenUH` int(11) NOT NULL,
  `XPersenUTS` int(11) NOT NULL,
  `XPersenUAS` int(11) NOT NULL,
  `XKKM` float NOT NULL,
  `XMapelAgama` char(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cbt_mapel`
--

INSERT INTO `cbt_mapel` (`Urut`, `XKodeKelas`, `XKodeMapel`, `XNamaMapel`, `XTglBuat`, `XPersenUH`, `XPersenUTS`, `XPersenUAS`, `XKKM`, `XMapelAgama`, `XKodeSekolah`) VALUES
(1, '', 'ML01', 'Muatan Lokal', '0000-00-00', 0, 0, 100, 65, 'N', ''),
(2, '', 'ML02', 'Muatan Lokal', '0000-00-00', 0, 0, 100, 65, 'N', ''),
(3, '', 'ML03', 'Muatan Lokal', '0000-00-00', 0, 0, 100, 65, 'N', '');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_nilai`
--

CREATE TABLE `cbt_nilai` (
  `Urut` int(11) NOT NULL,
  `XNomerUjian` varchar(20) NOT NULL,
  `XNIK` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeUjian` varchar(10) NOT NULL,
  `XTokenUjian` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTgl` date NOT NULL,
  `XJumSoal` int(11) NOT NULL,
  `XBenar` int(11) NOT NULL,
  `XSalah` int(11) NOT NULL,
  `XNilai` int(11) NOT NULL,
  `XPersenPil` float NOT NULL,
  `XPersenEsai` float NOT NULL,
  `XEsai` float NOT NULL,
  `XTotalNilai` float NOT NULL,
  `XKodeMapel` varchar(10) NOT NULL,
  `XKodeKelas` varchar(10) NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) NOT NULL,
  `XSemester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_paketsoal`
--

CREATE TABLE `cbt_paketsoal` (
  `Urut` int(11) NOT NULL,
  `XKodeKelas` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XLevel` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XPaketSoal` text COLLATE latin1_general_ci NOT NULL,
  `XSesi` int(11) NOT NULL DEFAULT '1',
  `XJenisSoal` int(11) NOT NULL,
  `XPilGanda` int(11) NOT NULL,
  `XEsai` int(11) NOT NULL,
  `XPersenPil` int(11) NOT NULL,
  `XPersenEsai` int(11) NOT NULL,
  `XKodeSoal` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XJumPilihan` int(11) NOT NULL DEFAULT '5',
  `XJumSoal` int(11) NOT NULL,
  `JumUjian` int(11) NOT NULL,
  `XAcakSoal` char(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `XGuru` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XSemua` enum('Y','T') COLLATE latin1_general_ci NOT NULL DEFAULT 'T',
  `XStatusSoal` varchar(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `XTglBuat` date NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cbt_paketsoal`
--

INSERT INTO `cbt_paketsoal` (`Urut`, `XKodeKelas`, `XLevel`, `XKodeJurusan`, `XKodeMapel`, `XPaketSoal`, `XSesi`, `XJenisSoal`, `XPilGanda`, `XEsai`, `XPersenPil`, `XPersenEsai`, `XKodeSoal`, `XJumPilihan`, `XJumSoal`, `JumUjian`, `XAcakSoal`, `XGuru`, `XSetId`, `XSemua`, `XStatusSoal`, `XTglBuat`, `XKodeSekolah`) VALUES
(1, 'XII TKJ', 'SMK', 'TKJ', 'ML01', '', 0, 0, 50, 0, 100, 0, 'ML01', 5, 50, 0, '', 'admin', '', 'T', 'N', '2017-03-23', ''),
(2, 'XII PH', 'SMK', 'Akomodasi Perhotelan', 'ML02', '', 0, 0, 50, 0, 100, 0, 'ML02', 5, 50, 0, '', 'admin', '', 'T', 'N', '2017-03-23', ''),
(3, 'XII BG', 'SMK', 'Jasa Boga', 'ML03', '', 0, 0, 50, 0, 100, 0, 'ML03', 5, 50, 0, '', 'admin', '', 'T', 'N', '2017-03-23', '');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_setid`
--

CREATE TABLE `cbt_setid` (
  `Urut` int(11) NOT NULL,
  `XKodeAY` varchar(10) NOT NULL,
  `XNamaAY` varchar(100) NOT NULL,
  `XStatus` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cbt_setid`
--

INSERT INTO `cbt_setid` (`Urut`, `XKodeAY`, `XNamaAY`, `XStatus`) VALUES
(1, '2016/2017', 'Tahun Ajaran 2016/2017', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_siswa`
--

CREATE TABLE `cbt_siswa` (
  `Urut` int(11) NOT NULL,
  `XNomerUjian` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XNIK` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XNamaSiswa` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeLevel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XJenisKelamin` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XPassword` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `XFoto` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `XAgama` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XSesi` int(11) NOT NULL,
  `XRuang` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XPilihan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XNoPeserta` varchar(25) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cbt_siswa`
--

INSERT INTO `cbt_siswa` (`Urut`, `XNomerUjian`, `XNIK`, `XKodeJurusan`, `XNamaSiswa`, `XKodeKelas`, `XKodeLevel`, `XJenisKelamin`, `XPassword`, `XFoto`, `XAgama`, `XSetId`, `XSesi`, `XRuang`, `XKodeSekolah`, `XPilihan`, `XNoPeserta`) VALUES
(1, 'K01041310018', '9971909377', 'TKJ', 'Aditya Prasetya', 'XII TKJ', 'XII', 'L', '271097*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(2, 'K01041310027', '9981121641', 'TKJ', 'Ahmad Alfarizki', 'XII TKJ', 'XII', 'L', '081198*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(3, 'K01041310036', '9991978461', 'TKJ', 'Bayu Nugroho', 'XII TKJ', 'XII', 'L', '190799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(4, 'K01041310045', '9977381531', 'TKJ', 'Dicki Firmansyah', 'XII TKJ', 'XII', 'L', '300697*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(5, 'K01041310054', '9989322497', 'TKJ', 'Erfansyah Tri Sofyandi', 'XII TKJ', 'XII', 'L', '300798*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(6, 'K01041310063', '9980695608', 'TKJ', 'Fajar Mashuda', 'XII TKJ', 'XII', 'L', '110498*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(7, 'K01041310072', '9983301209', 'TKJ', 'Fero Oktafian Susanto', 'XII TKJ', 'XII', 'L', '091098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(8, 'K01041310089', '9992813692', 'TKJ', 'Fikri Ramadhan', 'XII TKJ', 'XII', 'L', '051299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(9, 'K01041310098', '9991765955', 'TKJ', 'Mohammad Refly Nauval Mahera', 'XII TKJ', 'XII', 'L', '171199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(10, 'K01041310107', '9991803746', 'TKJ', 'Muamar Agung Ibrahim', 'XII TKJ', 'XII', 'L', '020399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(11, 'K01041310116', '9999876405', 'TKJ', 'Muhamad Reza Pratama', 'XII TKJ', 'XII', 'L', '200999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(12, 'K01041310125', '9997252962', 'TKJ', 'Muhammad Aldi Alant Permana', 'XII TKJ', 'XII', 'L', '250999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(13, 'K01041310134', '9979036030', 'TKJ', 'Muhammad Kemal Azhari', 'XII TKJ', 'XII', 'L', '301197*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(14, 'K01041310143', '9994828408', 'TKJ', 'Muhammad Nur Marwazi Azzuri', 'XII TKJ', 'XII', 'L', '060399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(15, 'K01041310152', '9992017142', 'TKJ', 'Muhammad Rezza Axl Sumantri', 'XII TKJ', 'XII', 'L', '070599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(16, 'K01041310169', '9995178795', 'TKJ', 'Muhammad Rivai Prakoso', 'XII TKJ', 'XII', 'L', '270199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(17, 'K01041310178', '9995005336', 'TKJ', 'Prayoga Putra Sandie Pratama', 'XII TKJ', 'XII', 'L', '010399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(18, 'K01041310187', '9991682932', 'TKJ', 'Priyo Budi Santoso', 'XII TKJ', 'XII', 'L', '110299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(19, 'K01041310196', '0005456513', 'TKJ', 'Rakha Nabil Zufar', 'XII TKJ', 'XII', 'L', '020200*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(20, 'K01041310205', '9997836778', 'TKJ', 'Ridwan Ighfirlana Ananda', 'XII TKJ', 'XII', 'L', '060799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(21, 'K01041310214', '9981997844', 'TKJ', 'Rizki Kurniawan', 'XII TKJ', 'XII', 'L', '310398*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(22, 'K01041310223', '9991974089', 'TKJ', 'Saipul Rahmat', 'XII TKJ', 'XII', 'L', '300699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(23, 'K01041310232', '9995258461', 'Akomodasi Perhotelan', 'Aceng Nurjumsari', 'XII PH', 'XII', 'L', '040699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(24, 'K01041310249', '9992017050', 'Akomodasi Perhotelan', 'Adela Nurahmawati', 'XII PH', 'XII', 'P', '200599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(25, 'K01041310258', '9992016721', 'Akomodasi Perhotelan', 'Adela Putri Utami', 'XII PH', 'XII', 'P', '250799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(26, 'K01041310267', '9987588714', 'Akomodasi Perhotelan', 'Andi Maulana', 'XII PH', 'XII', 'L', '190498*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(27, 'K01041310276', '9983301053', 'Akomodasi Perhotelan', 'Anwar Hidayatulloh', 'XII PH', 'XII', 'L', '261098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(28, 'K01041310285', '9994143968', 'Akomodasi Perhotelan', 'Bella Dwi Febryanti', 'XII PH', 'XII', 'P', '130299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(29, 'K01041310294', '9992011912', 'Akomodasi Perhotelan', 'Buswin Aril Johansyah', 'XII PH', 'XII', 'L', '110499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(30, 'K01041310303', '9991977351', 'Akomodasi Perhotelan', 'Dika Aristia Yuda', 'XII PH', 'XII', 'L', '300499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(31, 'K01041310312', '9992016815', 'Akomodasi Perhotelan', 'Dinnyta Agni Oktaviani', 'XII PH', 'XII', 'P', '191099*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(32, 'K01041310329', '9997491263', 'Akomodasi Perhotelan', 'Dondi Ramadhan', 'XII PH', 'XII', 'L', '070199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(33, 'K01041310338', '9982012838', 'Akomodasi Perhotelan', 'Ega Mawarnih', 'XII PH', 'XII', 'P', '230698*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(34, 'K01041310347', '9972031905', 'Akomodasi Perhotelan', 'Erza Noviansyah', 'XII PH', 'XII', 'L', '261197*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(35, 'K01041310356', '0001359715', 'Akomodasi Perhotelan', 'Fahmi Febriansyah', 'XII PH', 'XII', 'L', '200200*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(36, 'K01041310365', '9999783023', 'Akomodasi Perhotelan', 'Felia Rahmaika', 'XII PH', 'XII', 'P', '060199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(37, 'K01041310374', '9991977432', 'Akomodasi Perhotelan', 'Ferdy Fadilla', 'XII PH', 'XII', 'L', '240899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(38, 'K01041310383', '9981468020', 'Akomodasi Perhotelan', 'Luthfi Farhani Ramadhan', 'XII PH', 'XII', 'L', '140198*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(39, 'K01041310392', '9999737309', 'Akomodasi Perhotelan', 'Mohammad Fabel', 'XII PH', 'XII', 'L', '250399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(40, 'K01041310409', '9981102709', 'Akomodasi Perhotelan', 'Monica Septianggraini', 'XII PH', 'XII', 'P', '010998*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(41, 'K01041310418', '9998932062', 'Akomodasi Perhotelan', 'Mubibah', 'XII PH', 'XII', 'P', '300899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(42, 'K01041310427', '9999817365', 'Akomodasi Perhotelan', 'Muhammad Ridwan Firdaus', 'XII PH', 'XII', 'L', '170299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(43, 'K01041310436', '9992333094', 'Akomodasi Perhotelan', 'Raka Sasongko Nugraha', 'XII PH', 'XII', 'L', '180599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(44, 'K01041310445', '9997707910', 'Akomodasi Perhotelan', 'Rima Wulandari', 'XII PH', 'XII', 'P', '180799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(45, 'K01041310454', '9991976280', 'Akomodasi Perhotelan', 'Rindang Rizqurrohman', 'XII PH', 'XII', 'L', '210999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(46, 'K01041310463', '9969039706', 'Akomodasi Perhotelan', 'Siti Husnul Khotimah', 'XII PH', 'XII', 'P', '070896*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(47, 'K01041310472', '9975838506', 'Akomodasi Perhotelan', 'Tri Indraswari', 'XII PH', 'XII', 'P', '141297*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(48, 'K01041310489', '0003935263', 'Akomodasi Perhotelan', 'Tyas Farhan', 'XII PH', 'XII', 'L', '200300*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(49, 'K01041310498', '9998956877', 'Akomodasi Perhotelan', 'Wilda Anandya Haryasha', 'XII PH', 'XII', 'P', '250799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(50, 'K01041310507', '9994121119', 'Akomodasi Perhotelan', 'Yenni Ramadhanty Sonaz', 'XII PH', 'XII', 'P', '060199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(51, 'K01041310516', '9998183547', 'Akomodasi Perhotelan', 'Yoga Ananda Pratama', 'XII PH', 'XII', 'L', '220599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(52, 'K01041310525', '9991973959', 'Akomodasi Perhotelan', 'Ade Maisyah Rima', 'XII PH', 'XII', 'P', '050899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(53, 'K01041310534', '9982015545', 'Akomodasi Perhotelan', 'Ahmad Gerry Reynaldi', 'XII PH', 'XII', 'L', '300398*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(54, 'K01041310543', '9994600998', 'Akomodasi Perhotelan', 'Ahmad Hariri', 'XII PH', 'XII', 'L', '080599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(55, 'K01041310552', '9991976294', 'Akomodasi Perhotelan', 'Alda Mustika', 'XII PH', 'XII', 'P', '021299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(56, 'K01041310569', '9996304180', 'Akomodasi Perhotelan', 'Alfiansyah Halim', 'XII PH', 'XII', 'L', '230199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(57, 'K01041310578', '9991844112', 'Akomodasi Perhotelan', 'Andira Kusuma Putri', 'XII PH', 'XII', 'P', '130299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(58, 'K01041310587', '9992016618', 'Akomodasi Perhotelan', 'Arif Dede Yuliantono', 'XII PH', 'XII', 'L', '190799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(59, 'K01041310596', '9991991941', 'Akomodasi Perhotelan', 'Dandy Siswanto', 'XII PH', 'XII', 'L', '040499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(60, 'K01041310605', '9991997844', 'Akomodasi Perhotelan', 'Dela Latifah Nuri', 'XII PH', 'XII', 'P', '120899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(61, 'K01041310614', '9971300466', 'Akomodasi Perhotelan', 'Dody Jonpower', 'XII PH', 'XII', 'L', '161097*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(62, 'K01041310623', '9981991513', 'Akomodasi Perhotelan', 'Evie Rochmatus Suriyah', 'XII PH', 'XII', 'P', '261098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(63, 'K01041310632', '0004541003', 'Akomodasi Perhotelan', 'Fajrul Falah', 'XII PH', 'XII', 'L', '110100*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(64, 'K01041310649', '9991994271', 'Akomodasi Perhotelan', 'Fandi Ahmad', 'XII PH', 'XII', 'L', '300499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(65, 'K01041310658', '9991977445', 'Akomodasi Perhotelan', 'Kamil Prameza Afiadi', 'XII PH', 'XII', 'L', '151099*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(66, 'K01041310667', '9991687545', 'Akomodasi Perhotelan', 'Lukas Sitompul', 'XII PH', 'XII', 'L', '010799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(67, 'K01041310676', '9992069972', 'Akomodasi Perhotelan', 'Muhamad Chandra Syahnacri', 'XII PH', 'XII', 'L', '190999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(68, 'K01041310685', '9993212791', 'Akomodasi Perhotelan', 'Muhammad Erlangga Deandry', 'XII PH', 'XII', 'L', '150499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(69, 'K01041310694', '9982114576', 'Akomodasi Perhotelan', 'Muhammad Fikri Fadillah', 'XII PH', 'XII', 'L', '081098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(70, 'K01041310703', '9994122799', 'Akomodasi Perhotelan', 'Muhammad Rizal Arya Ibrani', 'XII PH', 'XII', 'L', '110799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(71, 'K01041310712', '9974966287', 'Akomodasi Perhotelan', 'Muhammad Yudith Firmanda', 'XII PH', 'XII', 'L', '230897*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(72, 'K01041310729', '9994122165', 'Akomodasi Perhotelan', 'Nada Aida', 'XII PH', 'XII', 'P', '110999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(73, 'K01041310738', '9992010534', 'Akomodasi Perhotelan', 'Noni Rama Prakosa', 'XII PH', 'XII', 'P', '040199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(74, 'K01041310747', '9999255350', 'Akomodasi Perhotelan', 'Raldo Baja Putra', 'XII PH', 'XII', 'L', '100899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(75, 'K01041310756', '9981102719', 'Akomodasi Perhotelan', 'Shinta Suryani', 'XII PH', 'XII', 'P', '181198*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(76, 'K01041310765', '9981468079', 'Akomodasi Perhotelan', 'Syafira Rahmasari', 'XII PH', 'XII', 'P', '041298*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(77, 'K01041310774', '9991977484', 'Akomodasi Perhotelan', 'Syifa Amalia', 'XII PH', 'XII', 'P', '180899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(78, 'K01041310783', '9979244891', 'Akomodasi Perhotelan', 'Ulil Absor', 'XII PH', 'XII', 'L', '150797*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(79, 'K01041310792', '0008546884', 'Akomodasi Perhotelan', 'Vinka Almaria', 'XII PH', 'XII', 'P', '180500*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(80, 'K01041310809', '9992428277', 'Akomodasi Perhotelan', 'Witha Khafifa', 'XII PH', 'XII', 'P', '080999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(81, 'K01041310818', '9980492086', 'Jasa Boga', 'Adi Anugrah Hertanto', 'XII BG', 'XII', 'L', '250998*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(82, 'K01041310827', '9985001000', 'Jasa Boga', 'Ahmad Fajar Siddiq', 'XII BG', 'XII', 'L', '121098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(83, 'K01041310836', '9994121231', 'Jasa Boga', 'Alifah Aprilia Lumban Tobing', 'XII BG', 'XII', 'P', '050499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(84, 'K01041310845', '9992010481', 'Jasa Boga', 'Alya Nuzriyah', 'XII BG', 'XII', 'P', '040199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(85, 'K01041310854', '9990867749', 'Jasa Boga', 'Aulia Asmarani', 'XII BG', 'XII', 'P', '140999*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(86, 'K01041310863', '9976896259', 'Jasa Boga', 'Cahyani', 'XII BG', 'XII', 'P', '161297*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(87, 'K01041310872', '9991994263', 'Jasa Boga', 'Dinindah A`ini', 'XII BG', 'XII', 'P', '220699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(88, 'K01041310889', '9988967978', 'Jasa Boga', 'Ergy Alivan', 'XII BG', 'XII', 'L', '190998*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(89, 'K01041310898', '9992428249', 'Jasa Boga', 'Erika Salsabila', 'XII BG', 'XII', 'P', '011099*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(90, 'K01041310907', '9992011670', 'Jasa Boga', 'Ferro Al Jabbar Santino', 'XII BG', 'XII', 'L', '120899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(91, 'K01041310916', '9991974469', 'Jasa Boga', 'Gita Fitri Rahmadhani', 'XII BG', 'XII', 'P', '160199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(92, 'K01041310925', '9981280517', 'Jasa Boga', 'Ilman Nurzaman', 'XII BG', 'XII', 'L', '151298*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(93, 'K01041310934', '9993019397', 'Jasa Boga', 'Imron Pratama', 'XII BG', 'XII', 'L', '101199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(94, 'K01041310943', '9992012386', 'Jasa Boga', 'Jelang Jurdiawan', 'XII BG', 'XII', 'L', '060699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(95, 'K01041310952', '9987748623', 'Jasa Boga', 'Krismawati Aurelia', 'XII BG', 'XII', 'P', '141098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(96, 'K01041310969', '9992011779', 'Jasa Boga', 'Mariska Anggraini', 'XII BG', 'XII', 'P', '260899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(97, 'K01041310978', '9997034973', 'Jasa Boga', 'Melyana Anggita', 'XII BG', 'XII', 'P', '130399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(98, 'K01041310987', '9999579584', 'Jasa Boga', 'Muhammad Khalil Deux Jevran', 'XII BG', 'XII', 'L', '050399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(99, 'K01041310996', '9996588592', 'Jasa Boga', 'Muhammad Mahdi', 'XII BG', 'XII', 'L', '060399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(100, 'K01041311005', '9991115059', 'Jasa Boga', 'Muhammad Ragil Ramadhan Tumin', 'XII BG', 'XII', 'L', '120199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(101, 'K01041311014', '9994846456', 'Jasa Boga', 'Nadella Salsabila Putri Prihadi', 'XII BG', 'XII', 'P', '280699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(102, 'K01041311023', '9998141915', 'Jasa Boga', 'Oki Akbarisya Priana Siger', 'XII BG', 'XII', 'L', '091099*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(103, 'K01041311032', '9983485186', 'Jasa Boga', 'Paskalis Triandika Aji', 'XII BG', 'XII', 'L', '020498*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(104, 'K01041311049', '9982011075', 'Jasa Boga', 'Prima Diana', 'XII BG', 'XII', 'P', '070298*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(105, 'K01041311058', '9986541615', 'Jasa Boga', 'Raflie Agam Viduhry', 'XII BG', 'XII', 'L', '050398*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(106, 'K01041311067', '9994122405', 'Jasa Boga', 'Syifa Khairunnisa', 'XII BG', 'XII', 'P', '201099*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(107, 'K01041311076', '9981925614', 'Jasa Boga', 'Velicia Rosana', 'XII BG', 'XII', 'P', '060298*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(108, 'K01041311085', '9983539980', 'Jasa Boga', 'Walid Dwiyana', 'XII BG', 'XII', 'L', '210298*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(109, 'K01041311094', '9997972014', 'Jasa Boga', 'Yusra Rayindra Putra', 'XII BG', 'XII', 'L', '230199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(110, 'K01041311103', '9992100114', 'Jasa Boga', 'Afriyani Krisniyati', 'XII BG', 'XII', 'P', '090499*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(111, 'K01041311112', '9985283160', 'Jasa Boga', 'Angelina Oktavia Sitompul', 'XII BG', 'XII', 'P', '081098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(112, 'K01041311129', '9991990542', 'Jasa Boga', 'Anissa Febryanti AM', 'XII BG', 'XII', 'P', '070299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(113, 'K01041311138', '9981508937', 'Jasa Boga', 'Deby Putri Gemilang', 'XII BG', 'XII', 'P', '120998*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(114, 'K01041311147', '9985506857', 'Jasa Boga', 'Delang Abidirar', 'XII BG', 'XII', 'L', '190298*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(115, 'K01041311156', '9991990476', 'Jasa Boga', 'Dhea Putri Ramadanti', 'XII BG', 'XII', 'P', '271299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(116, 'K01041311165', '9992016476', 'Jasa Boga', 'Endah Yunita Sulistyorini', 'XII BG', 'XII', 'P', '280699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(117, 'K01041311174', '9991977354', 'Jasa Boga', 'Ester Sekar Tajie', 'XII BG', 'XII', 'P', '190699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(118, 'K01041311183', '9995197958', 'Jasa Boga', 'Ibrohim Khotami', 'XII BG', 'XII', 'L', '020899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(119, 'K01041311192', '9981997980', 'Jasa Boga', 'Iksan Mugiono', 'XII BG', 'XII', 'L', '310198*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(120, 'K01041311209', '9980364574', 'Jasa Boga', 'Isnania Aryuni Zahra', 'XII BG', 'XII', 'P', '150698*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(121, 'K01041311218', '9985580474', 'Jasa Boga', 'Khusnul Chotimah', 'XII BG', 'XII', 'P', '310798*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(122, 'K01041311227', '9991974473', 'Jasa Boga', 'Lulu Nur Latifah', 'XII BG', 'XII', 'P', '270599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(123, 'K01041311236', '9992016779', 'Jasa Boga', 'Machfud Givary', 'XII BG', 'XII', 'L', '080299*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(124, 'K01041311245', '9991991003', 'Jasa Boga', 'Mita Dwi Ariescha', 'XII BG', 'XII', 'P', '290399*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(125, 'K01041311254', '9991977373', 'Jasa Boga', 'Muhammad Firdiansyah', 'XII BG', 'XII', 'L', '210699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(126, 'K01041311263', '9984549023', 'Jasa Boga', 'Muhammad Refal Ardiansyah', 'XII BG', 'XII', 'L', '100698*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(127, 'K01041311272', '9981508922', 'Jasa Boga', 'Nadyatul Maulida', 'XII BG', 'XII', 'P', '050398*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(128, 'K01041311289', '9981978825', 'Jasa Boga', 'Nilam Ratnasari', 'XII BG', 'XII', 'P', '301098*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(129, 'K01041311298', '9981054976', 'Jasa Boga', 'Nofanolo Christ Chanina Waruwu', 'XII BG', 'XII', 'L', '210398*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(130, 'K01041311307', '9982014868', 'Jasa Boga', 'Novianto Achmad Alfarizi', 'XII BG', 'XII', 'L', '221198*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(131, 'K01041311316', '9994121675', 'Jasa Boga', 'Pamungkas Hidayattullah', 'XII BG', 'XII', 'L', '290899*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(132, 'K01041311325', '9985398546', 'Jasa Boga', 'Rezza Giovani Herlambang', 'XII BG', 'XII', 'L', '190398*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(133, 'K01041311334', '9999519200', 'Jasa Boga', 'Rika Lestari', 'XII BG', 'XII', 'P', '270199*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(134, 'K01041311343', '9991410574', 'Jasa Boga', 'Rovi Muhamad Sidiq', 'XII BG', 'XII', 'L', '080599*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(135, 'K01041311352', '9998853396', 'Jasa Boga', 'Talitha Salsabila', 'XII BG', 'XII', 'P', '050799*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(136, 'K01041311369', '9995178808', 'Jasa Boga', 'Thia Nur Rahmadini', 'XII BG', 'XII', 'P', '190699*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', ''),
(137, 'K01041311378', '9974808130', 'Jasa Boga', 'Yuda Debriana', 'XII BG', 'XII', 'L', '251297*', '', '', '2016/2017', 1, '1 - 5', 'K0104131', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_siswa_ujian`
--

CREATE TABLE `cbt_siswa_ujian` (
  `Urut` int(11) NOT NULL,
  `XNomerUjian` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `XNISN` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XPilGanda` int(11) NOT NULL,
  `XEsai` int(11) NOT NULL,
  `XJumSoal` int(11) NOT NULL,
  `XTglUjian` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `XJamUjian` time NOT NULL,
  `XMulaiUjian` time NOT NULL,
  `XLastUpdate` time NOT NULL,
  `XSisaWaktu` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `XLamaUjian` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `XTargetUjian` time NOT NULL,
  `XTokenUjian` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `XSelesaiUjian` time NOT NULL,
  `XSetId` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeUjian` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XSesi` int(11) NOT NULL,
  `XStatusUjian` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XGetIP` varchar(20) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_soal`
--

CREATE TABLE `cbt_soal` (
  `Urut` int(11) NOT NULL,
  `XKodeMapel` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL DEFAULT '1',
  `XKodeKelas` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XLevel` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XNomerSoal` int(11) NOT NULL,
  `XRagu` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XTanya` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XAudioTanya` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XVideoTanya` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XGambarTanya` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab1` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab2` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab3` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab4` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab5` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `XGambarJawab1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XGambarJawab2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XGambarJawab3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XGambarJawab4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XGambarJawab5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XKunciJawaban` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XKategori` int(11) NOT NULL DEFAULT '1',
  `XAcakSoal` enum('A','T') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `XAcakOpsi` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XAgama` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cbt_soal`
--

INSERT INTO `cbt_soal` (`Urut`, `XKodeMapel`, `XKodeSoal`, `XJenisSoal`, `XKodeKelas`, `XLevel`, `XNomerSoal`, `XRagu`, `XTanya`, `XAudioTanya`, `XVideoTanya`, `XGambarTanya`, `XJawab1`, `XJawab2`, `XJawab3`, `XJawab4`, `XJawab5`, `XGambarJawab1`, `XGambarJawab2`, `XGambarJawab3`, `XGambarJawab4`, `XGambarJawab5`, `XKunciJawaban`, `XKategori`, `XAcakSoal`, `XAcakOpsi`, `XAgama`, `XKodeSekolah`) VALUES
(1, 'ML01', 'ML01', 1, '', '', 1, '', 'Apakah kepanjangan dari HTML?', '', '', '', 'Hight Text Mark Up Language', 'Hyper Text Marked Language', 'Hyper Text Manual Language', 'Hight Text Marked Language', 'Hyper Text Mark Up Language', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(2, 'ML01', 'ML01', 1, '', '', 2, '', 'Berikut ini adalah termasuk Software Browser, kecuali :', '', '', '', 'Mozilla Firefox', 'Opera', 'Netscape Navigator', 'Internet Explorer', 'Macromedia Dreamweaver', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(3, 'ML01', 'ML01', 1, '', '', 3, '', 'Klik untuk memperbesar gambar.', '', '', 'ml103.jpg', 'Membuat garis', 'Menulis teks pada baris berikutnya (ganti baris)', 'Mengatur ukuran', 'Membuat efek tulisan', 'Memasang background', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(4, 'ML01', 'ML01', 1, '', '', 4, '', 'Untuk memasukkan file gambar ke dalam web menggunakan perintah .....', '', '', '', '', '', '', '', '', 'ml104a.jpg', 'ml104b.jpg', 'ml104c.jpg', 'ml104d.jpg', 'ml104e.jpg', '5', 1, 'A', 'Y', '', ''),
(5, 'ML01', 'ML01', 1, '', '', 5, '', 'Berikut ini adalah Software untuk mendisain Halaman Web (Web Design Software), kecuali :', '', '', '', 'Adobe Image Ready', 'Macromedia Dreamweaver', 'Macromedia Fireworks', 'Microsoft Frontpage', 'Google Chrome', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(6, 'ML01', 'ML01', 1, '', '', 6, '', 'Berikut ini adalah Bahasa Pemrograman Web, kecuali :', '', '', '', 'PHP', 'JavaScript', 'CSS', 'EXL', 'XML', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(7, 'ML01', 'ML01', 1, '', '', 7, '', 'Type form HTML untuk menerima masukan berupa pilihan, dimana pilihan yang dipilih bisa lebih dari satu pilihan adalah .......', '', '', '', 'submit', 'radio', 'reset', 'button', 'checkbox', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(8, 'ML01', 'ML01', 1, '', '', 8, '', 'Salah satu halaman website statis adalah .....', '', '', '', 'php', 'html', 'asp', 'mysql', 'sql', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(9, 'ML01', 'ML01', 1, '', '', 9, '', 'Salah satu halaman website dinamis adalah .....', '', '', '', 'php', 'html', 'asp', 'mysql', 'sql', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(10, 'ML01', 'ML01', 1, '', '', 10, '', 'Untuk membuat tabel dengan jumlah kolom 3 dan baris 3, pada notepad harus ada tag .......', '', '', '', '', '', '', '', '', 'ml110a.jpg', 'ml110b.jpg', 'ml110c.jpg', 'ml110d.jpg', 'ml110e.jpg', '2', 1, 'A', 'Y', '', ''),
(11, 'ML01', 'ML01', 1, '', '', 11, '', 'Heading HTML mempunyai level ukuran huruf  .......', '', '', '', '1 sampai 10', '1 sampai 9', '1 sampai 8', '1 sampai 7', '1 sampai 6', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(12, 'ML01', 'ML01', 1, '', '', 12, '', 'Software untuk menulis bahasa HTML yang paling sederhana adalah ........', '', '', '', 'Ms Word', 'Ms Excel', 'Notepad', 'Ms Powerpoint', 'Ms Acces', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(13, 'ML01', 'ML01', 1, '', '', 13, '', 'Untuk memberi warna teks web diatur dengan mengubah nilai atribut  ..........', '', '', '', 'Body', 'Title', 'Fontcolor', 'Bgcolor', 'Head', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(14, 'ML01', 'ML01', 1, '', '', 14, '', 'Untuk memberi gambar pada belakang web diatur dengan mengubah nilai atribut  ............', '', '', '', 'Body', 'Body background', 'Bgcolor', 'Fontcolor', 'Head', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(15, 'ML01', 'ML01', 1, '', '', 15, '', 'Untuk memisahkan baris pada web dapat digunakan tag .....', '', '', '', '', '', '', '', '', 'ml115a.jpg', 'ml115b.jpg', 'ml115c.jpg', 'ml115d.jpg', 'ml115e.jpg', '2', 1, 'A', 'Y', '', ''),
(16, 'ML01', 'ML01', 1, '', '', 16, '', 'Untuk membuat garis horizontal didalam web dapat digunakan tag ....', '', '', '', '', '', '', '', '', 'ml116a.jpg', 'ml116b.jpg', 'ml116c.jpg', 'ml116d.jpg', 'ml116e.jpg', '3', 1, 'A', 'Y', '', ''),
(17, 'ML01', 'ML01', 1, '', '', 17, '', 'Untuk menampilkan gambar dalam halaman web maka gambar tersebut harus berformat ....', '', '', '', 'FLA', 'JPG', 'SWF', 'DOC', 'PDF', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(18, 'ML01', 'ML01', 1, '', '', 18, '', 'Klik untuk memperbesar gambar.', '', '', 'ml118.jpg', 'Mengubah gambar', 'Mengatur gambar', 'Memindahkan gambar', 'Menghapus gambar', 'Menampilkan gambar', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(19, 'ML01', 'ML01', 1, '', '', 19, '', 'Klik untuk memperbesar gambar.', '', '', 'ml119.jpg', 'HEIGHT, WIDTH', 'TYPE', 'BUTTON', 'SIZE, COLOR, FACE', 'SIZE , WIDTH', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(20, 'ML01', 'ML01', 1, '', '', 20, '', 'Yang bukan termasuk atribut elemen tabel adalah  .......', '', '', '', 'width', 'height', 'border', 'cellspacing', 'size', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(21, 'ML01', 'ML01', 1, '', '', 21, '', 'Tag yang digunakan untuk menampilkan informasi dalam bentuk list adalah .....', '', '', '', '', '', '', '', '', 'ml121a.jpg', 'ml121b.jpg', 'ml121c.jpg', 'ml121d.jpg', 'ml121e.jpg', '2', 1, 'A', 'Y', '', ''),
(22, 'ML01', 'ML01', 1, '', '', 22, '', 'Tag tabel adalah  .....', '', '', '', '', '', '', '', '', 'ml122a.jpg', 'ml122b.jpg', 'ml122b.jpg', 'ml122d.jpg', 'ml122e.jpg', '1', 1, 'A', 'Y', '', ''),
(23, 'ML01', 'ML01', 1, '', '', 23, '', 'Yang dimaksud dengan halaman Web adalah  ............', '', '', '', 'Halaman elektroni yang dibuka dengan email', 'Halaman digital yang berisi berbagai jenis data dan gambar', 'Halaman digital yang dibuka dengan web browser', 'Halaman digital online yang terhubung dengan internet', 'Halaman online bisa di download', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(24, 'ML01', 'ML01', 1, '', '', 24, '', 'Klik untuk memperbesar gambar.', '', '', 'ml124.jpg', 'disc', 'square', 'circle', 'disc dan square', 'number', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(25, 'ML01', 'ML01', 1, '', '', 25, '', 'Tombol yang berfungsi untuk melaksanakan pengisian dalam form adalah  .......:', '', '', '', 'reset', 'submit', 'preset', 'undo', 'button', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(26, 'ML01', 'ML01', 1, '', '', 26, '', 'Tombol yang berfungsi untuk membatalkan pengisian dalam form adalah .......', '', '', '', 'reset', 'submit', 'preset', 'undo', 'button', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(27, 'ML01', 'ML01', 1, '', '', 27, '', 'Perintah HTML dalam form yang digunakan untuk menerima masukan dari pengguna berupa beberapa baris teks, seperti komentar dalam guestbook adalah ....... ', '', '', '', '', '', '', '', '', 'ml127a.jpg', 'ml127b.jpg', 'ml127c.jpg', 'ml127d.jpg', 'ml127e.jpg', '5', 1, 'A', 'Y', '', ''),
(28, 'ML01', 'ML01', 1, '', '', 28, '', 'Untuk memberi nama atau title sebuah web/blog, harus disesuaikan dengan  .........', '', '', '', 'Desain web/blog', 'Koneksi internet yang digunakan', 'Nama penguna web/blog', 'Browser yang digunakan', 'Isi web/blog', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(29, 'ML01', 'ML01', 1, '', '', 29, '', 'Yang dimaksud dengan alamat web/blog adalah ....', '', '', '', 'URL', 'Link', 'Readmore', 'Label/Kategori', 'HTML', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(30, 'ML01', 'ML01', 1, '', '', 30, '', 'Berikut ini perinah HTML yang berhubungan dengan pembuatan tabel, kecuali : ', '', '', '', 'TD', 'TR', 'TS', 'TH', 'Table', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(31, 'ML01', 'ML01', 1, '', '', 31, '', 'Klik untuk memperbesar gambar.', '', '', 'ml131.jpg', 'Untuk memberikan teks smkwi.com pada halaman web', 'Untuk memberikan tombol menuju smkwi.com', 'Untuk memberikan tulisan pada smkwi.com', 'Untuk memberikan penomoran pada tulisan smkwi', 'Untuk memberikan format judul pada tulisan smkwi.com', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(32, 'ML01', 'ML01', 1, '', '', 32, '', 'Klik untuk memperbesar gambar.', '', '', 'ml132.jpg', 'isi sel dalam baris menjadi rata atas', 'isi sel dalam kolom menjadi rata atas', 'isi sel dalam kolom menjadi rata bawah', 'isi sel dalam baris menjadi rata bawah', 'isi sel dalam kolom dan baris menjadi rata atas', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(33, 'ML01', 'ML01', 1, '', '', 33, '', 'CSS merupakan singkatan dari  ..........', '', '', '', 'Cascading Sheet Style', 'Cascading Style Sheet', 'Conversion Sheet Style', 'Conversion Style Sheet', 'Conversion Select Sheet', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(34, 'ML01', 'ML01', 1, '', '', 34, '', 'Judul dalam halaman web diletakkan dalam tag .......', '', '', '', '', '', '', '', '', 'ml134a.jpg', 'ml134b.jpg', 'ml134c.jpg', 'ml134d.jpg', 'ml134e.jpg', '4', 1, 'A', 'Y', '', ''),
(35, 'ML01', 'ML01', 1, '', '', 35, '', 'Hal-hal pada penulisan script HTML yang tidak terpengaruh terhadap tampilan di halaman web  ...', '', '', '', 'Penulisan dengan huruf kapiral semua atau kecil semua', 'Penulisan dengan huruf besar dan kecil', 'Spasi sebanyak-banyaknya', 'Enter berkali kali atau ganti baris sebanyakbanyaknya', 'Penulisan titik atau koma', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(36, 'ML01', 'ML01', 1, '', '', 36, '', 'Tag yang digunakan untuk memainkan berkas yang berupa suara maupun video adalah  ....', '', '', '', '', '', '', '', '', 'ml136a.jpg', 'ml136b.jpg', 'ml136c.jpg', 'ml136d.jpg', 'ml136e.jpg', '3', 1, 'A', 'Y', '', ''),
(37, 'ML01', 'ML01', 1, '', '', 37, '', 'Bagaimanakah perintah HTML agar muncul input type password  .......', '', '', '', '', '', '', '', '', 'ml137a.jpg', 'ml137b.jpg', 'ml137c.jpg', 'ml137d.jpg', 'ml137e.jpg', '1', 1, 'A', 'Y', '', ''),
(38, 'ML01', 'ML01', 1, '', '', 38, '', 'Perintah unordered list agar dapat menampilkan list bulat titik adalah  .....', '', '', '', 'Circle', 'Disc', 'Donuts', 'Clyndrical', 'Discbox', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(39, 'ML01', 'ML01', 1, '', '', 39, '', 'Perintah untuk mengubah warna huruf menjadi merah dalam HTML adalah .....', '', '', '', '', '', '', '', '', 'ml139a.jpg', 'ml139b.jpg', 'ml139c.jpg', 'ml139d.jpg', 'ml139e.jpg', '5', 1, 'A', 'Y', '', ''),
(40, 'ML01', 'ML01', 1, '', '', 40, '', 'Untuk memberikan list a, b, c, dst menggunakan perintah .....', '', '', '', '', '', '', '', '', 'ml140a.jpg', 'ml140b.jpg', 'ml140c.jpg', 'ml140d.jpg', 'ml140e.jpg', '4', 1, 'A', 'Y', '', ''),
(41, 'ML01', 'ML01', 1, '', '', 41, '', 'Yang termasuk jenis daftar listing adalah .........', '', '', '', 'unordered list', 'ordered list', 'element list', 'a dan b benar', 'benar semua', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(42, 'ML01', 'ML01', 1, '', '', 42, '', 'Listing merupakan .........', '', '', '', 'menampilkan window baru pada web', 'menampilkan informasi dalam bentuk sel yang terdiri dari kolom dan baris', 'menampilkan informasi dalam bentuk daftar', 'menampilkan informasi dalam bentuk kotak', 'menampilkan sesuatu informasi dalam bentuk abstrak', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(43, 'ML01', 'ML01', 1, '', '', 43, '', 'Struktur dasar membuat tabel pada HTML adalah .....', '', '', '', '', '', '', '', '', 'ml143a.jpg', 'ml143b.jpg', 'ml143c.jpg', 'ml143d.jpg', 'ml143e.jpg', '3', 1, 'A', 'Y', '', ''),
(44, 'ML01', 'ML01', 1, '', '', 44, '', 'Kode RGB untuk warna biru adalah .....', '', '', '', '#FF0000', '#0000FF', '#000000', '#FFFFFF', '#00FFFF', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(45, 'ML01', 'ML01', 1, '', '', 45, '', 'Profesi-profesi berikut ini langsung terkait dalam pengembangan aplikasi web, kecuali :  ', '', '', '', 'web programmer', 'web administrator', 'web developer', 'web designer', 'web browser', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(46, 'ML01', 'ML01', 1, '', '', 46, '', 'Klik untuk memperbesar gambar.', '', '', 'ml146.jpg', 'memilih beberapa opsi', 'menentukan ukuran', 'menandai beberapa opsi', 'menampilkan opsi pilihan', 'membatalkan opsi', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(47, 'ML01', 'ML01', 1, '', '', 47, '', 'Klik untuk memperbesar gambar.', '', '', 'ml147.jpg', 'name', 'value', 'selected', 'size', 'action', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(48, 'ML01', 'ML01', 1, '', '', 48, '', 'Untuk mengulangi pengisian pada form menggunakan perintah ......', '', '', '', '', '', '', '', '', 'ml148a.jpg', 'ml148b.jpg', 'ml148c.jpg', 'ml148d.jpg', 'ml148e.jpg', '1', 1, 'A', 'Y', '', ''),
(49, 'ML01', 'ML01', 1, '', '', 49, '', 'Perintah untuk menyatukan 3 baris pada sebuah tabel adalah .....', '', '', '', '', '', '', '', '', 'ml149a.jpg', 'ml149b.jpg', 'ml149c.jpg', 'ml149d.jpg', 'ml149e.jpg', '5', 1, 'A', 'Y', '', ''),
(50, 'ML01', 'ML01', 1, '', '', 50, '', 'Apa fungsi dari type=â€passwordâ€ ?', '', '', '', 'jika kita mengetik sesuatu akan tertulis yang sebenarnya', 'jika kita mengetik sesuatu maka ketikan tersebut tidak terlihat', 'jika kita mengetik sesuatu maka yang muncul adalah angka', 'jika kita mengetik sesuatu maka yang muncul adalah ******', 'jika kita mengetik sesuatu tidak muncul apa-apa', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(51, 'ML02', 'ML02', 1, '', '', 1, '', 'Ciri-ciri coffee shop adalah â€¦', '', '', '', 'Harga relative murah', 'Makanannya dimasak di depan tamu', 'Makanannya ditata diatas plater', 'Lokasinya dilantai atas', 'Petugas restoran yang berpengalaman', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(52, 'ML02', 'ML02', 1, '', '', 2, '', 'Prosedur pelayanan coffee shop adalah ...', '', '', '', 'Memberikan kepuasan kepada tamunya', 'Table setting', 'Greeting the guest', 'Crumbing down', 'Mice en place', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(53, 'ML02', 'ML02', 1, '', '', 3, '', 'Contoh minuman pengiring makan adalah â€¦', '', '', '', 'Coffee atau tea', 'Soft drink,beer', 'Brandy', 'Dry sherry', 'Campari', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(54, 'ML02', 'ML02', 1, '', '', 4, '', 'Ciri khusus banquet adalah â€¦', '', '', '', 'Sanggup melayani jamuan makan dalam jumlah besar', 'Bisa untuk acara seminar', 'Bisa untuk wedding party', 'Bisa untuk halal bi halal', 'Bisa untuk pencarian dana', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(55, 'ML02', 'ML02', 1, '', '', 5, '', 'Penyelenggaraan event social di banquet antara lain â€¦', '', '', '', 'Peragaan busana', 'Pesta perkawinan', 'Perayaan malam tahun baru', 'Konferensi', 'Seminar', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(56, 'ML02', 'ML02', 1, '', '', 6, '', 'Penyelenggaraan event bisnis di banquet adalah....', '', '', '', 'Jamuan kenegaraan', 'Peragaan busana', 'Pesta malam dana', 'Musyawarah nasional', 'Pesta ulang tahun', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(57, 'ML02', 'ML02', 1, '', '', 7, '', 'Untuk menambah tenaga kerja banquet harus menghubungi department â€¦', '', '', '', 'Housekeeping', 'Engineering', 'Human resources department', 'Front office', 'Accounting', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(58, 'ML02', 'ML02', 1, '', '', 8, '', 'Di ruang rapat ada head table yaitu untuk â€¦', '', '', '', 'Peserta rapat', 'Panitia rapat', 'Pembawa acara (moderator)', 'Meja prasmanan', 'Meja alat-alat rapat', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(59, 'ML02', 'ML02', 1, '', '', 9, '', 'Untuk kebutuhan rapat(alat alat tulis) maka harus menghubungi department â€¦.', '', '', '', 'Store room', 'Kitchen', 'Steward', 'Accounting', 'Purchasing', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(60, 'ML02', 'ML02', 1, '', '', 10, '', 'Clear up adalah â€¦', '', '', '', 'Memasang dan menata peralatan makan', 'Mengambil dan memindahkan peralatan makan', 'Mengganti peralatan sesuai dengan makanan', 'Memberikan peralatan makan', 'Menghitung peralatan makan dan minuman', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(61, 'ML02', 'ML02', 1, '', '', 11, '', 'Room Service bagian dari Departemen â€¦', '', '', '', 'Food and Beverage', 'Engenering', 'Housekeeping', 'Akunting', 'Laundry', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(62, 'ML02', 'ML02', 1, '', '', 12, '', 'Pada saat Breakfast di Room Service biasanya tamu pesan melalui â€¦', '', '', '', 'Door Knob Menu', 'Cycle Menu', 'Table D Hote Menu', 'Static Menu', 'Actual Menu', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(63, 'ML02', 'ML02', 1, '', '', 13, '', 'Lembaran Menu makan pagi yang sudah dipilih tamunya antara lain â€¦', '', '', '', 'Cycle Menu', 'Table Dâ€™Hote Menu', 'Actual Menu', 'Static Menu', 'Door Knob Menu', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(64, 'ML02', 'ML02', 1, '', '', 14, '', 'Order taker di Room Service bertugas â€¦', '', '', '', 'Mengantar makanan ke kamar', 'Menerima pesanan dan menulis pesanan dari kamar', 'Clear up alat-alat makan dari kamar', 'Memesan mkanan dari kamar', 'Menanyakan bon (bill) darikamar', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(65, 'ML02', 'ML02', 1, '', '', 15, '', 'Pada saat pramusaji Room Service akan meninggalkan tamu kamar selalu mengucapkan pada tamunya â€¦.', '', '', '', 'Mohon menelepon kembali apabila selesai makan', 'Selamat menikmati dan terima kasih', 'Alat-alat makan setelah selesai dikeluarkan dari kamar ', 'Mohon periksa makanan yang ada', 'Mohon dibayar dimuka atau cash', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(66, 'ML02', 'ML02', 1, '', '', 16, '', 'Untuk menjaga keutuhan peralatan service, maka harus ditanamkan prinsip-prinsip â€¦', '', '', '', 'Ketelitian dalam pengantaran makanan ', 'Pramusaji yang melakukan clear up adalah pramusaji yang mengantar pesanan tamu', 'Pesanan makanan, melalui telepon', 'Jarak antara Room Service dan kamar sangat jauh', 'Lembaran Menu Breakfast', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(67, 'ML02', 'ML02', 1, '', '', 17, '', 'Pramusaji Room Service harus mempunyai â€¦', '', '', '', 'Ketelitian dan disipilin yang cukup tinggi ', 'Ketelitian dan jujur ', 'Ketelitian dan rajin ', 'Ketelitian dan mempunyai wawasan', 'Bahasa Inggris harus menguasai ', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(68, 'ML02', 'ML02', 1, '', '', 18, '', 'Penerima pesanan di Room Service dari kamar â€¦', '', '', '', 'Captain Waiter ', 'Hand waiter ', 'Order taker ', 'Anak-anak training', 'Waiter / waiters  ', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(69, 'ML02', 'ML02', 1, '', '', 19, '', 'Door knob menu adalah â€¦', '', '', '', 'Ala carte menu ', 'Lembaran menu breakfast', 'Brunch menu', 'American breakfast ', 'Continental Breakfast', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(70, 'ML02', 'ML02', 1, '', '', 20, '', 'Menu menurut waktu penyajian dari jam 15.00-17.00 WIB adalah menu â€¦', '', '', '', 'tea time', 'lunch', 'dinner', 'breakfast', 'brunch', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(71, 'ML02', 'ML02', 1, '', '', 21, '', 'apple pie termasuk salah satu contoh dari adonan kue  . . . ', '', '', '', 'cake', 'choux pastry', 'puff pastry', 'bakery', 'sugar dough', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(72, 'ML02', 'ML02', 1, '', '', 22, '', 'Untuk membuat croissant sebaiknya menggunakan tepung terigu berjenis. . . .', '', '', '', 'hard wheat', 'white wheat', 'whole wheat', 'soft wheat', 'medium wheat', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(73, 'ML02', 'ML02', 1, '', '', 23, '', 'Hal yang perlu diperhatikan pada saat mengoven kue adalahâ€¦', '', '', '', 'merk oven', 'bahan bakar oven', 'suhu oven', 'model oven', 'tipe oven', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(74, 'ML02', 'ML02', 1, '', '', 24, '', 'zat yang dihasilkan dari pemecahan antara gula oleh ragi, kecualiâ€¦', '', '', '', 'gas CO2', 'basa', 'asam', 'alcohol', 'panas', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(75, 'ML02', 'ML02', 1, '', '', 25, '', 'Di bawah ini merupakan isian pada cream horns, yaitu  . . .', '', '', '', 'jeruk nipis', 'coklat', 'pastry cream', 'selai', 'buah-buahan', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(76, 'ML02', 'ML02', 1, '', '', 26, '', 'Memiliki karakteristik ringan dan bervolume besar serta dikembangkan dengan kuat, adalah ciri dari . . . ', '', '', '', 'choux pastry', 'danish pastry', 'phyllo', 'puff pastry', 'cake', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(77, 'ML02', 'ML02', 1, '', '', 27, '', 'lemak yang dapat memberikan lapisan pada adonan pastry adalah .....', '', '', '', 'telur', 'margarin', 'mentega', 'butter', 'corsvet', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(78, 'ML02', 'ML02', 1, '', '', 28, '', 'Fungsi pengadukan dalam tahap proses pembuatan bakery antara lainâ€¦', '', '', '', 'untuk mencegah tercemarnya roti dari bakteri', 'untuk memungkinkan pemotongan tampa alami kerusakan ', 'memberi bentuk supaya mudah dikerjakan', 'untuk mencampur secara rata semua bahan ', 'menstabilkan gluten', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(79, 'ML02', 'ML02', 1, '', '', 29, '', 'Fungsi ragi dalam pembuatan adoan roti adalah sebagaiâ€¦', '', '', '', 'sebagai pelumas untuk mengembangkan sel yang akan memperbaiki tekstur', 'menstabilakan gluten', 'pembentuk elmusi bdan pemberi warna pada kue', 'memberi rasa manis', 'mengembangkan adonan menghasilkan gas CO2 ', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(80, 'ML02', 'ML02', 1, '', '', 30, '', 'Memiliki karakteristik ringan dan bervolume besar serta dikembangkan dengan kuat, adalah ciri dari . . . ', '', '', '', 'choux pastry', 'danish pastry', 'phyllo', 'puff pastry', 'cake', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(81, 'ML02', 'ML02', 1, '', '', 31, '', 'Berikut ini yang termasuk dalam contoh produk cake adalahâ€¦', '', '', '', 'pound cake dan puff pastry', 'strudel dan phyllo', 'danish pastry dan chiffon cake ', 'sponge cake dan chiffon cake', 'short paste dan Danish pastry', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(82, 'ML02', 'ML02', 1, '', '', 32, '', 'Fungsi gula dalam pengolahan pastry adalah . . .', '', '', '', 'menstabilkan gluten', 'pemberi warna pada kulit kue', 'pengontrol suhu adonan', 'pelarut adonan', 'memberi tekstur berlapis-lapis', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(83, 'ML02', 'ML02', 1, '', '', 33, '', '(1) mengempukan produk\n<br> (2) pembentukan warna kulit\n<br> (3) memberi aroma\n<br> (4) sebagai pengawet \n<br>Peryataan di atas merupakan fungsi dari bahanâ€¦ \n', '', '', '', 'garam', 'telur', 'gula', 'tepung', 'baking powder', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(84, 'ML02', 'ML02', 1, '', '', 34, '', 'Gula pasir yang digiling halus seperti tepung adalah. . . ', '', '', '', 'granulated sugar', 'palm sugar', 'castor sugar ', 'honey', 'icing sugar', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(85, 'ML02', 'ML02', 1, '', '', 35, '', 'Alat persiapan yang digunakan pada saat pembuatan pastry adalahâ€¦', '', '', '', 'rolling pin ', 'mixer', 'oven', 'scale', 'slicer', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(86, 'ML02', 'ML02', 1, '', '', 36, '', 'Berdasarkan jenisnya hidangan pembuka dibagi menjadi . . . ', '', '', '', 'white appetizer dan brown appetizer ', 'cold appetizer dan hot appetizer ', 'nasional appetizer dan internasional appetizer', 'clear appetizer dan thick appetizer', 'simple appetizer dan complex appetizer', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(87, 'ML02', 'ML02', 1, '', '', 37, '', 'Teknik pengolahan salad adalah . . . ', '', '', '', 'mixing', 'baking', 'grilling', 'steaming', 'poaching', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(88, 'ML02', 'ML02', 1, '', '', 38, '', 'Porsi salad sebagai hidangan appetizer. . . ', '', '', '', '40-50gram', '80-100gram', '80-125gram ', '125-150gram', '250-300gram', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(89, 'ML02', 'ML02', 1, '', '', 39, '', 'Dibawah ini yang bukan merupakan teknik pengolahan dalam membuat stock adalah â€¦', '', '', '', 'boiling', 'simmer', 'roasting', 'frying', 'baking', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(90, 'ML02', 'ML02', 1, '', '', 40, '', 'Boiling adalah metode memasak dengan menggunakan. . . ', '', '', '', 'sedikit minyak', 'sedikit air ', 'tanpa menggunakan minyak dan air', 'banyak air', 'uap panas', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(91, 'ML02', 'ML02', 1, '', '', 41, '', 'Di bawah ini yang merupakan bahan untuk mire poix adalah . . . ', '', '', '', 'carrot ', 'thymme ', 'bayleave ', 'oregano', 'Parsley', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(92, 'ML02', 'ML02', 1, '', '', 42, '', 'Urutkan proses pembuatan kaldu yang tepat, yaitu  . . .\n<br>1. Preparing          \n <br>2. Cooking\n<br>3. Saving  \n<br>4. Skimmering  \n<br>5. Blancing\n<br>6. Simmering \n', '', '', '', '1-5-2-4-6-3', '1-5-2-6-4-3', '5-1-2-4-6-3 ', '5-1-2-6-4-3', '5-1-3-2-4-6', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(93, 'ML02', 'ML02', 1, '', '', 43, '', 'Salad yang terdiri 1 atau 2 macam sayuran dari jenis yang sama disebut . .', '', '', '', 'coumpound salad', 'simple salad', 'american salad', 'fruit salad', 'mix salad', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(94, 'ML02', 'ML02', 1, '', '', 44, '', 'Sup kental yang dikentalkan dengan menggunakan bahan pengental cream atau tepung disebutâ€¦', '', '', '', 'potage', 'bisque', 'chowder', 'puree soup', 'cream soup', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(95, 'ML02', 'ML02', 1, '', '', 45, '', 'Bahan dasar pasta terbuat dari tepungâ€¦', '', '', '', 'ketan', 'tapioka', 'kanji', 'semolina', 'beras', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(96, 'ML02', 'ML02', 1, '', '', 46, '', 'Al Dente adalah istilah untuk pasta yangâ€¦ ', '', '', '', 'kenyalnya pas', 'ukurannya pas', 'rasanya pas', 'warnaya pas', 'harganya pas', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(97, 'ML02', 'ML02', 1, '', '', 47, '', 'Hidangan pasta yang dibuat menyusun kulit pasta yang telah direbus diberi siraman meat sauce dan bÃ©chamel sauce adalahâ€¦', '', '', '', 'cannelloni', 'spaghetti', 'pane', 'lasagna', 'pizza', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(98, 'ML02', 'ML02', 1, '', '', 48, '', 'Jenis sauce yang digunakan untuk membuat pasta fettucini adalahâ€¦', '', '', '', 'marinara sauce', 'mornay sauce', 'bolognaise sauce', 'tomato sauce', 'congsace sauce', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(99, 'ML02', 'ML02', 1, '', '', 49, '', 'Bahan dasar pembuatan bÃ©chamel sauce adalahâ€¦', '', '', '', 'tepung, santan, telur ', 'tepung, telur, kaldu', 'tepung, fresh milk, kaldu', 'tepung, cream, kaldu', 'tepung, santan, cream', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(100, 'ML02', 'ML02', 1, '', '', 50, '', 'Fungsi sauce pada hidangan pasta adalag sebagai kecualiâ€¦', '', '', '', 'menambah nilai gizi', 'menetralisir rasa', 'Memperindah tampilan', 'memperkaya aroma dan rasa', 'memperpanjang daya simpan', '', '', '', '', '', '5', 1, 'A', 'Y', '', ''),
(101, 'ML03', 'ML03', 1, '', '', 1, '', 'Suatu bar yang menyediakan minuman alcohol serta bukan alcohol disamping itu tersedia music life serta tersedia dance floor yaituâ€¦.', '', '', '', 'Discotique Bar', 'Publik Bar ', 'Pub Bar ', 'Service Bar', 'Bauquet Bar', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(102, 'ML03', 'ML03', 1, '', '', 2, '', 'Bar yang menyediakan segala jenis minuman alcohol dan non alcohol.Contoh Cocktail Bar dan Night Club adalahâ€¦', '', '', '', 'Bauquet Bar', 'Publik Bar ', 'Pub Bar ', 'Discotique Bar', 'Service Bar', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(103, 'ML03', 'ML03', 1, '', '', 3, '', 'Bar yang menyediakan alcoholic dan non alcoholic drink juga tersedia snack/makanan kecil terdapat floor dance dengan diiringi music tape control/music jockeyâ€¦', '', '', '', 'Publik Bar ', 'Bauquet Bar', 'Pub Bar ', 'Discotique Bar', 'Service Bar', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(104, 'ML03', 'ML03', 1, '', '', 4, '', 'Bar yang khusus melayani tamu diluar counter bar ( Restoran, Room Service, dan lain-lain) adalahâ€¦.', '', '', '', 'Service Bar', 'Publik Bar ', 'Bauquet Bar', 'Discotique Bar', 'Pub Bar ', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(105, 'ML03', 'ML03', 1, '', '', 5, '', 'Publik bar lebih dikenal oleh kamu dengan sebutanâ€¦.', '', '', '', 'Service Bar', 'Pub Bar ', 'Bauquet Bar', 'Discotique Bar', 'Bar Personal', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(106, 'ML03', 'ML03', 1, '', '', 6, '', 'Bar ini berfungsi melayani pesanan-pesanan tamu, baik tamu di dalam kamar maupun di restoran adalahâ€¦.', '', '', '', 'Bauquet Bar', 'Service Bar', 'Pub Bar ', 'Discotique Bar', 'Publik Bar ', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(107, 'ML03', 'ML03', 1, '', '', 7, '', 'Petugas bar harus memiliki kepribadian yang baik itu termasukâ€¦', '', '', '', 'Dispence Bar', 'Bar Personal', 'Service Bar', 'Publik Bar ', 'Counter Bar', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(108, 'ML03', 'ML03', 1, '', '', 8, '', 'Personal Apperance yang menarik dan menyenangkan termasukâ€¦', '', '', '', 'Bar Personal', 'Dispence Bar', 'Service Bar', 'Publik Bar ', 'Counter Bar', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(109, 'ML03', 'ML03', 1, '', '', 9, '', 'Tempat memajang atau menyusun minuman dalam botol adalahâ€¦', '', '', '', 'Counter Area', 'Bar Display', 'Guest Area', 'Cashier Desk Area', 'Disk Washing Area', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(110, 'ML03', 'ML03', 1, '', '', 10, '', 'Bertanggung jawab kepada management, menjaga kebersihan dan kerapihan bar termasukâ€¦.', '', '', '', 'Spesifikasi seorang bartender', 'Kraian umum tugas dan tanggung jawab ', 'Tugas-tugas bar personal', 'Preparation', 'Spesifikasi bar waiter', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(111, 'ML03', 'ML03', 1, '', '', 11, '', 'Store keeper aula adalahâ€¦.', '', '', '', 'Tempat memajang dan menyusunminuman dalam botol', 'Tempat bar equipment', 'Tempat menyimpan persedian dan botol kosong', 'Tempat pencucian gelas', 'Tempat pencucian gelas', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(112, 'ML03', 'ML03', 1, '', '', 12, '', 'Kebiasaan bartender tidak suka alcohol, tidak suka mengorek telinga diarea bar itu termasukâ€¦', '', '', '', 'Tugas dan tanggung jawab seorang bartender', 'Kemampuan mengenal jenis minuman', 'Spesifikasi seorang bartender', 'Memiliki kepribadian yang menarik', 'Personal Apperance', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(113, 'ML03', 'ML03', 1, '', '', 13, '', 'Tugas-tugas yang harus dilaksanakan oleh petugas di suatu bar pada waktu mempersiapkan bar sebelum beroperasinya yaituâ€¦.', '', '', '', 'Closing Time', 'Bar operation', 'Preparation', 'Guest Area', 'Store Keeper Area', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(114, 'ML03', 'ML03', 1, '', '', 14, '', 'Tempat bertugas seorang bartender adalahâ€¦', '', '', '', 'Bar Counter', 'Area Counter', 'Working Bench', 'Bar Display', 'Back Bar', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(115, 'ML03', 'ML03', 1, '', '', 15, '', 'Ruang / aula tempat kerja bartender dan peralatan untuk mencampur minuman serta penyedian-penyedian garnish, es batu dan lain-lain adalahâ€¦', '', '', '', 'Bar Display', 'Back Bar', 'Working Bench', 'Bar Display', 'Area Counter', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(116, 'ML03', 'ML03', 1, '', '', 16, '', 'Jigger atau gelas ukuran termasukâ€¦.', '', '', '', 'Bar Utensilk', 'Garnish dan supplies', 'Glass wear', 'Bar Display', 'Supplier bar', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(117, 'ML03', 'ML03', 1, '', '', 17, '', 'Yang termasuk garnish di bar adalahâ€¦', '', '', '', 'Tabasco sauce', 'Telor ayam kampong', 'Pala', 'Buah zaitun', 'simple syrup', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(118, 'ML03', 'ML03', 1, '', '', 18, '', 'Supplier  atau persedian lain yang juga dipergunakan di bar selain garnish yaituâ€¦.', '', '', '', 'Buah jeruk', 'Buah Strawbery', 'Buah tomat', 'Garam dan merica ', 'Buah nanas', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(119, 'ML03', 'ML03', 1, '', '', 19, '', 'Botol minuman di bar didisplay, antara lain, kelompok whiskeyâ€¦.', '', '', '', 'Vermount', 'Bourbon', 'Brandy', 'Vodka', 'Sherry', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(120, 'ML03', 'ML03', 1, '', '', 20, '', 'Botol minuman di bar didisplay antara lain kelompok Aperitive adalahâ€¦', '', '', '', 'Sherry', 'Rum', 'Cognac', 'Cardials', 'Canadian', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(121, 'ML03', 'ML03', 1, '', '', 21, '', 'Pelayanan di bar adalah pada saat tamu datang di sambut olehâ€¦.', '', '', '', 'Captain', 'Bar Waiter', 'Bar Greeter', 'Bartender', 'Bar Manager', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(122, 'ML03', 'ML03', 1, '', '', 22, '', 'Pada saat closing time sebaiknya bartender memberikan kesempatan terakhir ( last Call ) sebelum bar ditutup kira-kira adalahâ€¦.', '', '', '', '15 menit', '25 menit', '30 menit', '60 menit', 'pada saatnya langsung tutup', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(123, 'ML03', 'ML03', 1, '', '', 23, '', 'Ada beberapa langkah penting untuk mengahadapi ketidakpuasan tamuâ€¦', '', '', '', 'Tunjukan bahwa anda dapat memakluminy a', 'Melaksanakan Clear Up dari seluruh meja-meja', 'Merapikan semua botol-botol di bar', 'Mencuci dan membersihkan semua peralatan', 'Biasakan malakukan pemeriksaan ulang', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(124, 'ML03', 'ML03', 1, '', '', 24, '', 'Bartender bekerja di dalamâ€¦.', '', '', '', 'Guest Aula', 'Counter Aula', 'Cashier Desk', 'Side Standar', 'Display Aula', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(125, 'ML03', 'ML03', 1, '', '', 25, '', 'Dalam bar ada lemari kecil yang dipergunakan untuk menyimpan/ menaruh alat-alat yang diperlukan untuk pelaksanaan service di bar yaituâ€¦.', '', '', '', 'Counter Aula', 'Guest Aula', 'Cashier Desk', 'Side Standar', 'Display Aula', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(126, 'ML03', 'ML03', 1, '', '', 26, '', 'Ada beberapa fungsi atau manfaat minuman bagi tubuh kita diantaranyaâ€¦', '', '', '', 'Untuk mengurangi nafsu makan', 'Untuk menghilangkan rasa haus', 'Untuk menguatkan tubuh', 'Uutuk mengurai kalori dan energy', 'Untuk mengurangi pencernaan', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(127, 'ML03', 'ML03', 1, '', '', 27, '', 'Jenis minuman yang tidak mengandung alcoholâ€¦..', '', '', '', 'Crush', 'Wine', 'Beer', 'Stout', 'Scoth', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(128, 'ML03', 'ML03', 1, '', '', 28, '', 'Jenis minuman yang mengandung alcoholâ€¦', '', '', '', 'Natural mineral water', 'Juice', 'Beer', 'Squash', 'Coffee', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(129, 'ML03', 'ML03', 1, '', '', 29, '', 'Jenis minuman yang diproses dengan cara peragian dan penyulinganâ€¦â€¦', '', '', '', 'Whisky', 'Aromatize wine', 'Beer', 'Sake', 'Stout', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(130, 'ML03', 'ML03', 1, '', '', 30, '', 'Jenis minuman yang diproses dengan cara peragian sepertiâ€¦â€¦', '', '', '', 'Whisky', 'Fortified', 'Vodka', 'Scoth', 'Rye', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(131, 'ML03', 'ML03', 1, '', '', 31, '', 'Minuman dingin yang disiapkan di Bar antara lainâ€¦..', '', '', '', 'White wine', 'Milk shake', 'Es teh', 'Es coffee', 'Air es', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(132, 'ML03', 'ML03', 1, '', '', 32, '', 'Minuman dingin yang disiapkan di kitchen antara lainâ€¦.', '', '', '', 'Milk shake', 'Coca cola', '7up', 'Tonic water', 'Mineral water', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(133, 'ML03', 'ML03', 1, '', '', 33, '', 'Minuman penyegar dari hasil perasan buah dengan menambahkan gula dan disajikan dengan daging buahnya.Minuman tersebut adalahâ€¦..', '', '', '', 'Squash', 'Crush', 'Syirup', 'Fruit juice', 'Stimulant drinks', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(134, 'ML03', 'ML03', 1, '', '', 34, '', 'Dalam membuat minuman campuran,telur ayam,juice,cream,soda water termasukâ€¦.', '', '', '', 'Garnish', 'Bahan dasar atau basic ingredients', 'Bahan pelunak rasa atau modify agents', 'Bahan penambah rasa', 'Aperitif', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(135, 'ML03', 'ML03', 1, '', '', 35, '', 'Membuat minuman dengan cara mengaduk didalam gelas penyaji sebelum minuman disajikan adalahâ€¦.', '', '', '', 'Shaking', 'Stirring', 'Mixing', 'Blending', 'Floating', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(136, 'ML03', 'ML03', 1, '', '', 36, '', 'Membuat minuman campuran dengan cara menuangkan langsung kedalam gelas.Contoh whisky soda,gin tonic adalahâ€¦.', '', '', '', 'Preparing', 'Stirring', 'Shaking', 'Mixing', 'Blending', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(137, 'ML03', 'ML03', 1, '', '', 37, '', 'Singapore sling adalah termasuk minumanâ€¦â€¦', '', '', '', 'Long drinks', 'High ball', 'Cocktail', 'Collins', 'Punch', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(138, 'ML03', 'ML03', 1, '', '', 38, '', ' termasuk minuman campuranâ€¦', '', '', '', 'Long drinks', 'High ball', 'Cocktail', 'Collins', 'Slings', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(139, 'ML03', 'ML03', 1, '', '', 39, '', 'Minuman campuran yang menggunakan bahan dasar Gin dan spirit contohnya,whisky sour,gin sour,vodka sour,dan disajikan dengan sourglass adalahâ€¦..', '', '', '', 'Slings', 'Punch', 'Collins', 'Sour', 'Juice', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(140, 'ML03', 'ML03', 1, '', '', 40, '', 'Minuman ini bahan dasarnya adalah barley/malt,bunga hops,gula air,dan bahan penjernih(finings)â€¦..', '', '', '', 'Whisky', 'Brandy', 'Beer', 'Cocnag', 'Juice', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(141, 'ML03', 'ML03', 1, '', '', 41, '', 'Beck beer,lowenbrak,dortmunder,hoflan jenis beer dari Negaraâ€¦.', '', '', '', 'Amerika', 'Indonesia', 'Germany', 'Philipina', 'Perancis', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(142, 'ML03', 'ML03', 1, '', '', 42, '', 'Beer sangat cocok untuk mengiringi hidanganâ€¦', '', '', '', 'Ayam panggang', 'Ayam goreng', 'Ayam semur', 'Ayam gule', 'Ayam goreng tepung', '', '', '', '', '', '2', 1, 'A', 'Y', '', ''),
(143, 'ML03', 'ML03', 1, '', '', 43, '', 'Mana yang bukan gelas untuk beerâ€¦â€¦.', '', '', '', 'Beer mug', 'Pilsner glass', 'Flure', 'Wter goble', 'Collins', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(144, 'ML03', 'ML03', 1, '', '', 44, '', 'Beer harus disimpan pada suhuâ€¦â€¦', '', '', '', '18 derajat celcius', '4-7 derajat celcius', '10 derajat celcius', '8 derajat celcius', '6 derajat celcius', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(145, 'ML03', 'ML03', 1, '', '', 45, '', 'Jepang penghasil beerâ€¦', '', '', '', 'Bintang', 'Anker', 'San Miguel', 'Kirin', 'Heinikens', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(146, 'ML03', 'ML03', 1, '', '', 46, '', 'Sebutkan negara-negara penghasil Brandyâ€¦..', '', '', '', 'Amerika', 'Inggris', 'Italy', 'Chilie', 'Indonesia', '', '', '', '', '', '1', 1, 'A', 'Y', '', ''),
(147, 'ML03', 'ML03', 1, '', '', 47, '', 'Dari negara mana Champagne berasalâ€¦..', '', '', '', 'Amerika', 'Arab Saudi', 'Francis', 'Spanyol', 'Inggris', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(148, 'ML03', 'ML03', 1, '', '', 48, '', 'Jenis minuman yang dibuat dengan penggabungan dari bitter dan aromatic essence dengan bahan alcohol base adalahâ€¦.', '', '', '', 'Tequila', 'Absinthe', 'Aquavit', 'Bitters', 'Beer', '', '', '', '', '', '4', 1, 'A', 'Y', '', ''),
(149, 'ML03', 'ML03', 1, '', '', 49, '', 'Sebutkan 3 warna rumâ€¦.', '', '', '', 'White,golden,blue', 'Golden,silver,red', 'White,golden,dark', 'Silver,blue,white', 'Red,white,blue', '', '', '', '', '', '3', 1, 'A', 'Y', '', ''),
(150, 'ML03', 'ML03', 1, '', '', 50, '', 'Wine hasil dari fermentasi berganda atau dua kali fermentasi atau re-fermentasi dari white wine disebutâ€¦..', '', '', '', 'Natural wine', 'Sparkling wine', 'Fortified wine', 'Aromatic wine', 'Rose wine', '', '', '', '', '', '2', 1, 'A', 'Y', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_tes`
--

CREATE TABLE `cbt_tes` (
  `Urut` int(11) NOT NULL,
  `XKodeUjian` varchar(10) NOT NULL,
  `XNamaUjian` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cbt_tes`
--

INSERT INTO `cbt_tes` (`Urut`, `XKodeUjian`, `XNamaUjian`) VALUES
(1, 'UH', 'Ulangan Harian'),
(2, 'UTS', 'Ujian Tengah Semester'),
(3, 'UAS', 'Ujian Akhir Semester'),
(5, 'TO1', 'Try Out I'),
(6, 'TO2', 'Try Out II'),
(7, 'TO3', 'Try Out III'),
(8, 'TO4', 'Try Out IV'),
(9, 'TO5', 'Try Out V');

-- --------------------------------------------------------

--
-- Table structure for table `cbt_tugas`
--

CREATE TABLE `cbt_tugas` (
  `Urut` int(11) NOT NULL,
  `XLevel` varchar(10) NOT NULL,
  `XNIK` varchar(10) NOT NULL,
  `XKodeMapel` varchar(10) NOT NULL,
  `XKodeKelas` varchar(10) NOT NULL,
  `XKodeJurusan` varchar(50) NOT NULL,
  `XSemester` int(11) NOT NULL,
  `XSetId` varchar(10) NOT NULL,
  `XNilaiTugas` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_ujian`
--

CREATE TABLE `cbt_ujian` (
  `Urut` int(11) NOT NULL,
  `XKodeUjian` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XSemester` int(11) NOT NULL,
  `XLevel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XLambat` enum('0','1') COLLATE latin1_general_ci NOT NULL,
  `XJumPilihan` int(11) NOT NULL,
  `XJumSoal` int(11) NOT NULL,
  `XPilGanda` int(11) NOT NULL,
  `XEsai` int(11) NOT NULL,
  `XAcakSoal` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XTglUjian` date NOT NULL,
  `XJamUjian` time NOT NULL,
  `XBatasMasuk` time NOT NULL,
  `XSisaWaktu` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `XLamaUjian` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `XIdleTime` int(11) NOT NULL,
  `XTokenUjian` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `XGuru` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `XTglBuat` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `XStatusUjian` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `XPengawas` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `XNIPPengawas` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `XSesi` int(11) NOT NULL,
  `XKodeSekolah` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `XCatatan` text COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_upload_file`
--

CREATE TABLE `cbt_upload_file` (
  `Urut` int(11) NOT NULL,
  `XNamaFile` varchar(250) NOT NULL,
  `XFolder` varchar(250) NOT NULL,
  `XCreated` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_user`
--

CREATE TABLE `cbt_user` (
  `Urut` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `NIP` varchar(30) NOT NULL,
  `Nama` varchar(200) NOT NULL,
  `HP` varchar(20) NOT NULL,
  `FacebookID` varchar(100) NOT NULL,
  `login` int(11) NOT NULL,
  `Status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cbt_user`
--

INSERT INTO `cbt_user` (`Urut`, `Username`, `Password`, `NIP`, `Nama`, `HP`, `FacebookID`, `login`, `Status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', '', '', 1, '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cbt_admin`
--
ALTER TABLE `cbt_admin`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_audio`
--
ALTER TABLE `cbt_audio`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_jawaban`
--
ALTER TABLE `cbt_jawaban`
  ADD PRIMARY KEY (`Urutan`);

--
-- Indexes for table `cbt_kelas`
--
ALTER TABLE `cbt_kelas`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_mapel`
--
ALTER TABLE `cbt_mapel`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_nilai`
--
ALTER TABLE `cbt_nilai`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_paketsoal`
--
ALTER TABLE `cbt_paketsoal`
  ADD PRIMARY KEY (`Urut`),
  ADD KEY `Urut` (`Urut`);

--
-- Indexes for table `cbt_setid`
--
ALTER TABLE `cbt_setid`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_siswa`
--
ALTER TABLE `cbt_siswa`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_siswa_ujian`
--
ALTER TABLE `cbt_siswa_ujian`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_soal`
--
ALTER TABLE `cbt_soal`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_tes`
--
ALTER TABLE `cbt_tes`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_tugas`
--
ALTER TABLE `cbt_tugas`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_ujian`
--
ALTER TABLE `cbt_ujian`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_upload_file`
--
ALTER TABLE `cbt_upload_file`
  ADD PRIMARY KEY (`Urut`);

--
-- Indexes for table `cbt_user`
--
ALTER TABLE `cbt_user`
  ADD PRIMARY KEY (`Urut`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cbt_admin`
--
ALTER TABLE `cbt_admin`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cbt_audio`
--
ALTER TABLE `cbt_audio`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_jawaban`
--
ALTER TABLE `cbt_jawaban`
  MODIFY `Urutan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_kelas`
--
ALTER TABLE `cbt_kelas`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cbt_mapel`
--
ALTER TABLE `cbt_mapel`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cbt_nilai`
--
ALTER TABLE `cbt_nilai`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_paketsoal`
--
ALTER TABLE `cbt_paketsoal`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cbt_setid`
--
ALTER TABLE `cbt_setid`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cbt_siswa`
--
ALTER TABLE `cbt_siswa`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `cbt_siswa_ujian`
--
ALTER TABLE `cbt_siswa_ujian`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_soal`
--
ALTER TABLE `cbt_soal`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `cbt_tes`
--
ALTER TABLE `cbt_tes`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cbt_tugas`
--
ALTER TABLE `cbt_tugas`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_ujian`
--
ALTER TABLE `cbt_ujian`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_upload_file`
--
ALTER TABLE `cbt_upload_file`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_user`
--
ALTER TABLE `cbt_user`
  MODIFY `Urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
